<section id="one">
  <h2>Blog entries</h2>
  <div>This blog is the starting point for sharing my interests and insights. I look forward to filling it with more in-depth content in the near future.</div>
  <div class="darkerhr"></div>
  <br>
  <div class="post">
    <h3 class="title">Peerprint: A modern preprint server?</h3>
    <p class="metadata">
        <span class="highlight">Author:</span> Martijn Hidding, 
        <span class="highlight">Published on:</span> 13 September 2023.
    </p>
    <div class="lighterhr"></div>
    <p class="postsummary">I believe that the publication system in the current academic landscape has room for improvement. In this post, I want to briefly summarize some of my thoughts on the subject and propose an improved git-based preprint repository system, for which I currently envisage the name <u>Peerprint</u>.
    </p>
    <div class="expandable-content">
    <div class="postminiheading">Introduction</div>
    <p>In theoretical physics, as well as numerous other technical disciplines, <a href="https://arxiv.org/">arXiv.org</a> serves as the primary preprint repository. Alternative but similar platforms like medrXiv exist for the medical community, and most of my comments here apply generally. A cursory glance at arXiv.org reveals its antiquated design and outdated technology stack. While such aesthetic shortcomings are a minor gripe, they are not my primary concern. I believe, however, that the workings of the system itself are dated in the same way that its visual layer is.</p>
    <p>The main drawback that I feel with arXiv.org is that it employs a fixed versioning scheme. While this serves the purpose of archiving and encourages thorough submissions, submitting multiple versions may inadvertently signal incomplete or hastily conducted research. Consequently, persistent inaccuracies often become 'open secrets' within specialized subfields, yet remain uncorrected on arXiv.org and unknown in the broader academic community. This leads to redundant errors and wastes researchers' time as they independently grapple with the same issues.</p>
    
    <div class="postminiheading">How Version Control (GIT) Could Help</div>
    <p>Interestingly, we could draw some inspiration here from software engineering where similar challenges have already been solved through the use of Git and other version control systems. These systems support tagged releases for reference but also maintain a dynamic main branch for continuous and tracked improvements. A platform like 'Peerprint' would draw inspiration from this model by building a preprint server where the papers are hosted as Git repositories. The system would have multiple advantages.</p>
    <p>Firstly, the use of Git would encourage researchers to make small incremental improvements to their papers and to more elaborately document the changes that were made. Furthermore, the use of Git would lower the threshold for including code together with the publication. While arXiv.org allows one to put files in the metadata of a paper, these can be difficult to access, and similarly, any updates to the code would require uploading a new version of the paper. For this reason, code that belongs to a given paper is already often hosted outside of arXiv.org on a platform such as GitHub or GitLab, and it seems sensible that the paper is hosted alongside the code in a similar fashion.</p>
    <p>My current vision of 'Peerprint' would not be to fully recreate the git-based platform of GitHub. Instead, the system will categorize and collect preprints just as arXiv.org does. However, different from arXiv.org, every paper will be closely linked to a git repository that can be hosted externally. Both the PDF and paper metadata should be hosted on the external repository, and by default, the latest version will be shown. However, tagged releases will be copied over to the 'Peerprint' server for archival purposes and can be viewed within the system itself. Any publication on 'Peerprint' should include at least one tagged release v1.</p>
    <p>Platforms like GitHub enable users to flag issues, categorized as resolved, closed, or in-progress. Adopting this feature in an academic context could serve multiple purposes. It would formalize the correction process, making the 'insider knowledge' of errors and typos widely accessible. This, in turn, would incentivize ongoing refinement of papers. Issue tracking should preferably be hosted on 'Peerprint', although mirroring from external repositories could also be considered for greater flexibility.</p>
    <p>An exact blueprint for integrating peer review with 'Peerprint' warrants a separate discussion, but one viable approach could be the establishment of specialized boards for specific research areas. Composed of senior researchers, these boards would oversee the peer-review pipeline directly on the 'Peerprint' website, and assign referees for papers which are sent in for review. In line with practices on platforms like openreview.net—used in machine learning and computer science—the (anonymous) referee reports would be visible to the public.</p>
    <div class="postminiheading">Peerprint: A Hybrid Solution</div>
    <p>In summary, I suggest a new type of preprint server, for which I currently envisage the name 'Peerprint'. It improves on arXiv.org in the following ways:</p>
    <ul>
        <li>Papers are associated with git repositories, allowing for incremental updates. Releases of papers can be marked for bibliographic purposes.</li>
        <li>The repository has a section for every paper where issues can be raised, similar to how it works for software repositories. Issues can be marked as closed, in-progress, or resolved.</li>
        <li>In the future: the preprint server has a selection of scientific boards, which provide open peer-review. The boards are run by senior researchers in the community and focus on particular topics. The moderation and assignment of peer-reviewers are conducted by board members, similar to scientific journals.</li>
    </ul>
    <p>I'm keen to hear your insights on Peerprint. If the concept resonates with you and you're interested in collaborating to bring this idea to fruition, I'd be eager to discuss next steps.</p>
</div>
<div class="lighterhr"></div>
<div class="expand-controls">
    <span class="expand-icon">&#9660;</span>
    <span class="expand-text">Expand</span>
  </div>
</div>  


</section>  